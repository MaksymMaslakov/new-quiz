import Vue from 'vue'
import Router from 'vue-router'

import TestsPage from './pages/TestsPage'
import CreateTestPage from './pages/CreateTestPage'
import EditTestPage from './pages/EditTestPage'
import PassTestPage from './pages/PassTestPage'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: {
        name: 'tests'
      }
    },
    {
      path: '/tests',
      component: { render: h => h('router-view') },
      children: [
        {
          path: '',
          name: 'tests',
          component: TestsPage
        },
        {
          path: 'create',
          name: 'tests.create',
          component: CreateTestPage
        },
        {
          path: 'edit/:id',
          name: 'tests.edit',
          component: EditTestPage
        },
        {
          path: 'pass/:id',
          name: 'tests.pass',
          component: PassTestPage
        }
      ]
    },
    {
      path: '/404',
      name: 'notFound',
      component: { render: createElement => createElement('h1', ['404']) }
    },
    {
      path: '/*',
      redirect: {
        name: 'notFound'
      }
    }

  ]
})

export default router
