export default {
  strict: true,
  namespaced: true,
  state: {
    test: {
      id: null,
      user_id: null,
      title: '',
      questions: []
    },
    isPosting: false,
    isFetching: false,
    error: null
  },
  actions: {
    fetchTestById ({ commit }, { service, id }) {
      commit('FETCH_TEST_REQUEST')

      service.getTestById(id)
        .then(test => commit('FETCH_TEST_SUCCESS', test))
        .catch(error => commit('FETCH_TEST_FAILURE', error))
    },
    saveTest ({ commit, state }, { service, method }) {
      commit('POST_TEST_REQUEST')

      service.saveTestOnBackend(state.test, method)
        .then(test => commit('POST_TEST_SUCCESS', test))
        .catch(error => {
          commit('POST_TEST_FAILURE', error)
        })
    }
  },
  mutations: {
    FETCH_TEST_REQUEST (state) {
      state.isFetching = true
    },
    FETCH_TEST_SUCCESS (state, test) {
      state.test = test
      state.isFetching = false
      state.error = null
    },
    FETCH_TEST_FAILURE (state, error) {
      state.test = {
        id: null,
        user_id: null,
        title: '',
        questions: []
      }
      state.isFetching = false
      state.error = error
    },
    POST_TEST_REQUEST (state) {
      state.isPosting = true
    },
    POST_TEST_SUCCESS (state) {
      state.test = {
        id: null,
        user_id: null,
        title: '',
        questions: []
      }
      state.isPosting = false
      state.error = null
    },
    POST_TEST_FAILURE (state, error) {
      state.isPosting = false
      state.error = error
    },
    UPDATE_TEST (state, test) {
      state.test = test
      state.isFetching = false
      state.isPosting = false
      state.error = null
    }
  }
}
