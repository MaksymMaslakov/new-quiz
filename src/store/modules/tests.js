export default {
  strict: true,
  namespaced: true,
  state: {
    tests: [],
    isFetching: false,
    error: null
  },
  actions: {
    async fetchAllTests ({ commit }, service) {
      commit('FETCH_ALL_TEST_REQUEST')

      return await service.getAllTests()
        .then(tests => commit('FETCH_ALL_TEST_SUCCESS', tests))
        .catch(error => commit('FETCH_ALL_TEST_FAILURE', error))
    },
    async deleteTest ({ commit }, { service, idForDeleteTest }) {
      // here have to be service method for delete test from database
      commit('DELETE_TEST', idForDeleteTest)
    }
  },
  mutations: {
    FETCH_ALL_TEST_REQUEST (state) {
      state.isFetching = true
    },
    FETCH_ALL_TEST_SUCCESS (state, tests) {
      state.tests = tests
      state.isFetching = false
      state.error = null
    },
    FETCH_ALL_TEST_FAILURE (state, error) {
      state.tests = []
      state.isFetching = false
      state.error = error
    },
    DELETE_TEST (state, idForDeleteTest) {
      const idx = state.tests.findIndex(test => test.id === idForDeleteTest)
      state.tests.splice(idx, 1)
    }
  }
}
