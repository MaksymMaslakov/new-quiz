export default {
  strict: true,
  namespaced: true,
  state: {
    profile: {
      id: null,
      email: '',
      google_id: '',
      first_name: '',
      last_name: '',
      picture: null
    },
    isLoggedIn: false,
    isFetching: false,
    error: null
  },
  actions: {
    async fetchProfile ({ commit }, service) {
      commit('FETCH_PROFILE_REQUEST')

      return await service.fetchUserProfile()
        .then(user => commit('FETCH_PROFILE_SUCCESS', user))
        .catch(error => commit('FETCH_PROFILE_FAILURE', error))
    },

    async isSessionExist ({ commit }, service) {
      return await service.fetchUserProfile()
        .then(data => commit('CHANGE_IS_LOGGED_IN', !!data))
        .catch(error => {
          commit('CHANGE_IS_LOGGED_IN', false)
          commit('FETCH_PROFILE_FAILURE', error)
        })
    }
  },
  mutations: {
    FETCH_PROFILE_REQUEST (state) {
      state.isFetching = true
      state.profile = {
        id: null,
        email: '',
        google_id: '',
        first_name: '',
        last_name: '',
        picture: null
      }
      state.error = null
    },

    FETCH_PROFILE_SUCCESS (state, payload) {
      state.isFetching = false
      state.profile = payload
      state.error = null
    },

    FETCH_PROFILE_FAILURE (state, payload) {
      state.isFetching = false
      state.profile = {
        id: null,
        email: '',
        google_id: '',
        first_name: '',
        last_name: '',
        picture: null
      }
      state.error = payload
    },

    CHANGE_IS_LOGGED_IN (state, payload) {
      state.isLoggedIn = payload
    }
  }
}
