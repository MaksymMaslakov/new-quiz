export default {
  strict: true,
  namespaced: true,
  state: {
    test: {
      id: null,
      userId: null,
      title: '',
      questions: []
    },
    userAnswers: [],
    isFetching: false,
    error: null
  },
  actions: {
    fetchTestByIdForPassing ({ commit }, { service, id }) {
      commit('FETCH_TEST_REQUEST')

      service.getTestByIdForPassing(id)
        .then(test => commit('FETCH_TEST_SUCCESS', test))
        .catch(error => commit('FETCH_TEST_FAILURE', error))
    }
  },
  mutations: {
    FETCH_TEST_REQUEST (state) {
      state.isFetching = true
    },
    FETCH_TEST_SUCCESS (state, test) {
      state.test = test
      state.isFetching = false
      state.error = null
    },
    FETCH_TEST_FAILURE (state, error) {
      state.test = {
        id: null,
        userId: null,
        title: '',
        questions: []
      }
      state.isFetching = false
      state.error = error
    },
    UPDATE_USER_ANSWERS (state, payload) {
      state.userAnswers = payload
    }
  }
}
