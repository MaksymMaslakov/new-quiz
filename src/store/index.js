import Vue from 'vue'
import Vuex from 'vuex'

import testPass from './modules/testPass'
import testForm from './modules/testForm'
import tests from './modules/tests'
import user from './modules/user'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    testPass,
    testForm,
    tests,
    user
  }
})
