const BASE_URL = 'http://localhost:4444'

export default class QuizService {
  constructor () {
    this.getAllTests = this.getAllTests.bind(this)
    this.getTestById = this.getTestById.bind(this)
    this.saveTestOnBackend = this.saveTestOnBackend.bind(this)
    this.fetchUserProfile = this.fetchUserProfile.bind(this)
    this.getTestByIdForPassing = this.getTestByIdForPassing.bind(this)
  }

  async getAllTests () {
    const res = await fetch(`${BASE_URL}/tests`)

    if (!res.ok) {
      throw new Error(`Could not fetch, received ${res.status}`)
    }

    return await res.json()
  }

  async getTestById (id) {
    const res = await fetch(`${BASE_URL}/tests/${id}`)

    if (!res.ok) {
      throw new Error(`Could not fetch, received ${res.status}`)
    }

    return await res.json()
  }

  async getTestByIdForPassing (id) {
    const res = await fetch(`${BASE_URL}/tests/pass/${id}`)

    if (!res.ok) {
      throw new Error(`Could not fetch, received ${res.status}`)
    }

    return await res.json()
  }

  async saveTestOnBackend (test, method) {
    let str = ''

    /// //////////////////////////////  FOR REAL SERVICE
    // if (method === 'POST') { str = '/create' }
    // if (method === 'PUT') { str = `/edit/${test.id}` }

    /// //////////////////////////////  FOR MOCK SERVER
    if (method === 'POST') { str = '' }
    if (method === 'PUT') { str = `/${test.id}` }

    const res = await fetch(
      `${BASE_URL}/tests${str}`,
      {
        method,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(test)
      })

    if (!res.ok) {
      throw new Error(`Could not fetch, received ${res.status}`)
    }

    return await res.json()
  }

  async fetchUserProfile () {
    const res = await fetch(`${BASE_URL}/users/profile`)

    if (!res.ok) {
      throw new Error(`Could not fetch, received ${res.status}`)
    }

    return await res.json()
  }
}
