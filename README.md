# frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development frontend side with mock data from mock-server 
```
npm run start-with-mock-server
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
